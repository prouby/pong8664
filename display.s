## display.s

## Draw pixel on _screen
##  params  int %rdi (x)
##  params  int %rsi (y)
##  params  int %rdx (color_index)
##  returns int %rax
draw_pixel:
        pushq   %rdx            # Save arg 3

        movq    $640, %rax
        imulq   %rsi            # iy = y * (320 * sizeof(short))
        movq    %rax, %rsi      # Save result on %rsi

        movq    $2, %rax
        imulq   %rdi            # ix = x * sizeof(short)
        movq    %rax, %rdi      # Save result on %rdi

        addq    %rsi, %rdi      # index = ix + iy

        leaq    _screen(%rip), %rdx
        movq    (%rdx), %rax    # Get screen pointer
        addq    %rdi, %rax      # screen[index]

        popq    %rdx
        cmp     $0, %dx
        je      draw_pixel_end
        mov     %dx, (%rax)      # screen[index] = 1
draw_pixel_end:
        xor     %rax,%rax
        ret


## Draw vertical line on _screen
##  params  int %rdi (x)
##  params  int %rsi (y)
##  returns int %rax
draw_vertical_line_8:
        cmpq    $320, %rdi      # x > 320px
        jg      draw_vertical_line_8_end
        movq    %rdi, %r8       # Save x on r8

        cmpq    $240, %rsi      # y > 240px
        jg      draw_vertical_line_8_end
        movq    %rsi, %r9       # Save y on r9

        xor     %rcx, %rcx      # Init lines (y) counter
draw_vertical_line_8_line:
        cmpq    $8, %rcx
        je      draw_vertical_line_8_end

        ## Draw pixel
        movq    %r8, %rdi       # Get start x
        movq    %r9, %rsi       # Get start y
        addq    %rcx, %rsi      # add y step
        movq    $1, %rdx        # Color 1
        call    draw_pixel      # Draw

        addq    $1, %rcx        # Add line
        jmp     draw_vertical_line_8_line
draw_vertical_line_8_end:
        xor     %rax, %rax
        ret


## Draw tex on _screen[x,y]
##  params  short* %rdi (tex)
##  params  int    %rsi (x)
##  params  int    %rdx (y)
##  returns int    %rax
draw_tex:
        pushq   %r12

        cmpq    $0, %rdi
        je      draw_tex_end
        movq    %rdi, %r10
        addq    $4, %r10

        cmpq    $320, %rsi      # x > 320px
        jg      draw_tex_end
        movq    %rsi, %r8       # Save x on r8

        cmpq    $240, %rdx      # y > 240px
        jg      draw_tex_end
        movq    %rdx, %r9       # Save y on r9

        ## Start draw
        xor     %r12, %r12      # Init lines (y) counter
        jmp     draw_tex_line
draw_tex_next_line:
        addq    $0x1, %r12      # Add line (y += 1)
draw_tex_line:
        xor     %r11, %r11      # Init x index counter
        movq    %r10, %rax
        subq    $2, %rax
        mov     (%rax), %dx
        cmpq    %rdx, %r12      # h pixels
        je      draw_tex_end
draw_tex_on_line:
        movq    %r10, %rax
        subq    $4, %rax
        mov     (%rax), %dx
        cmpq    %rdx, %r11      # w pixels
        je      draw_tex_next_line
        ## tex[x+y*w]
        movq    %rdx, %rax      # w
        addq    %rax, %rax      # w = w * sizeof(short)
        imulq   %r12            # y*w
        movq    %rax, %rdx      # keep result on %rdx
        movq    %r11, %rax      # x
        addq    %rax, %rax      # x * 2 (2 = sizeof(short))
        addq    %rdx, %rax      # x + y*w
        addq    %r10, %rax      # tex[x + y*w]
        mov     (%rax), %dx
        cmp     $0, %dx         # Skip drawing if color == 0
        jle     draw_tex_skip_draw
        ## Draw pixel
        movq    %r8, %rdi       # Get start x
        addq    %r11, %rdi      # add x step
        movq    %r9, %rsi       # Get start y
        addq    %r12, %rsi      # add y step

        call    draw_pixel      # Draw
draw_tex_skip_draw:
        addq    $0x1, %r11      # x += 1
        jmp     draw_tex_on_line
draw_tex_end:
        popq    %r12
        xor     %rax, %rax
        ret


## Draw vertical line on _screen
##  params  short* %rdi (tex)
##  params  int    %rsi (x)
##  params  int    %rdx (y)
##  params  int    %rcx (color)
##  returns int    %rax
draw_tex_with_color:
        pushq   %r12
        pushq   %r13

        cmpq    $0, %rdi
        je      draw_tex_with_color_end
        movq    %rdi, %r10
        addq    $4, %r10

        cmpq    $320, %rsi      # x > 320px
        jg      draw_tex_with_color_end
        movq    %rsi, %r8       # Save x on r8

        cmpq    $240, %rdx      # y > 240px
        jg      draw_tex_with_color_end
        movq    %rdx, %r9       # Save y on r9

        cmpq    $0, %rcx        # c == 0
        je      draw_tex_with_color_end
        movq    %rcx, %r13      # Save color on r13

        ## Start draw
        xor     %r12, %r12      # Init lines (y) counter
        jmp     draw_tex_with_color_line
draw_tex_with_color_next_line:
        addq    $0x1, %r12      # Add line (y += 1)
draw_tex_with_color_line:
        xor     %r11, %r11      # Init x index counter
        movq    %r10, %rax
        subq    $2, %rax
        mov     (%rax), %dx
        cmpq    %rdx, %r12      # h pixels
        je      draw_tex_with_color_end
draw_tex_with_color_on_line:
        movq    %r10, %rax
        subq    $4, %rax
        mov     (%rax), %dx
        cmpq    %rdx, %r11      # w pixels
        je      draw_tex_with_color_next_line
        ## tex[x+y*w]
        movq    %rdx, %rax      # w
        addq    %rax, %rax      # w = w * sizeof(short)
        imulq   %r12            # y*w
        movq    %rax, %rdx      # keep result on %rdx
        movq    %r11, %rax      # x
        addq    %rax, %rax      # x * 2 (2 = sizeof(short))
        addq    %rdx, %rax      # x + y*w
        addq    %r10, %rax      # tex[x + y*w]
        mov     (%rax), %dx
        cmp     $0, %dx         # Skip drawing if color == 0
        jle     draw_tex_with_color_skip_draw
        ## Overwrite color
        movq    %r13, %rax
        mov     %ax, %dx
        ## Draw pixel
        movq    %r8, %rdi       # Get start x
        addq    %r11, %rdi      # add x step
        movq    %r9, %rsi       # Get start y
        addq    %r12, %rsi      # add y step

        call    draw_pixel      # Draw
draw_tex_with_color_skip_draw:
        addq    $0x1, %r11      # x += 1
        jmp     draw_tex_with_color_on_line
draw_tex_with_color_end:
        popq    %r13
        popq    %r12
        xor     %rax, %rax
        ret

## display.s ends here.
