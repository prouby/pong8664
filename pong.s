# pong.s --- Pong 8664.                                   -*- mode: asm -*-
#
# Copyright (C) 2019  Pierre-Antoine Rouby
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

        .file "pong.s"

        ##########
        ## DATA ##
        ##########

        .section	.rodata
_str_license:   .string "GPLv3+"
_str_name:      .string "Pong 8664"

.include "colors.s"
.include "sprites.s"

        #####################
        ## GLOBAL VARIABLE ##
        #####################

        .section        .data
_rand_next:     .quad 0x1
_rand_max:      .quad 32767

## Game global variable
_screen:        .quad 0x0
_input:         .quad 0x0

_status:        .quad 0x0
_frames:        .quad 0x0

## Players score
_score:
_score_p1:      .quad 0
_score_p2:      .quad 0

## Players pos
_p1_y:          .quad 104
_p2_y:          .quad 104

## Ball
_ball_x:        .quad 152
_ball_y:        .quad 112
_ball_speed_x:  .quad -2
_ball_speed_y:  .quad 1

        ###############
        ## FUNCTIONS ##
        ###############

        .section        .text
	.global	        license, name, init, render, color

.include "display.s"

## Returns texture pointer on %rax corresponding to number on %rdi.
##  param     int   %rdi
##  returns   void* %rax
get_num_tex:
        cmpq    $0, %rdi
        je      get_num_tex_0
        cmpq    $1, %rdi
        je      get_num_tex_1
        cmpq    $2, %rdi
        je      get_num_tex_2
        cmpq    $3, %rdi
        je      get_num_tex_3
        cmpq    $4, %rdi
        je      get_num_tex_4
        cmpq    $5, %rdi
        je      get_num_tex_5
        cmpq    $6, %rdi
        je      get_num_tex_6
        cmpq    $7, %rdi
        je      get_num_tex_7
        cmpq    $8, %rdi
        je      get_num_tex_8

        jmp     get_num_tex_9
get_num_tex_0:
        leaq    _0(%rip), %rax
        jmp     get_num_tex_end
get_num_tex_1:
        leaq    _1(%rip), %rax
        jmp     get_num_tex_end
get_num_tex_2:
        leaq    _2(%rip), %rax
        jmp     get_num_tex_end
get_num_tex_3:
        leaq    _3(%rip), %rax
        jmp     get_num_tex_end
get_num_tex_4:
        leaq    _4(%rip), %rax
        jmp     get_num_tex_end
get_num_tex_5:
        leaq    _5(%rip), %rax
        jmp     get_num_tex_end
get_num_tex_6:
        leaq    _6(%rip), %rax
        jmp     get_num_tex_end
get_num_tex_7:
        leaq    _7(%rip), %rax
        jmp     get_num_tex_end
get_num_tex_8:
        leaq    _8(%rip), %rax
        jmp     get_num_tex_end
get_num_tex_9:
        leaq    _9(%rip), %rax
get_num_tex_end:
        ret

## Reset game variable.
##  params   void
##  returns  void
reset_game:
        ## Reset players pos
        leaq    _p1_y(%rip), %rax
        leaq    _p2_y(%rip), %rcx
        movq    $104, (%rax)
        movq    $104, (%rcx)

        ## Reset ball x pos and y speed
        leaq    _ball_x(%rip), %rax
        leaq    _ball_speed_y(%rip), %rdx
        movq    $152, (%rax)
        movq    $1, (%rdx)

        ## Random y pos
        leaq    _ball_y(%rip), %rcx
        movq    (%rcx), %rax
        salq    $3, %rax        # * 8
        addq    $18929, %rax    # + 18929
        movq    $232, %rsi      # % 200
        xor     %rdx, %rdx
        div     %rsi
        movq    %rdx, (%rcx)

        ## Switch x dir
        leaq    _ball_speed_x(%rip), %rcx
        movq    (%rcx), %rax
        cmpq    $0, %rax
        jl      reset_game_x
        movq    $-2, (%rcx)
        ret
reset_game_x:
        movq    $2, (%rcx)
        ret

        ####################
        # PUBLIC FUNCTIONS #
        ####################

## Retruns license string pointer
##  params  void
##  returns char* %rax
license:
        leaq    _str_license(%rip), %rax
        ret


## Retruns name string pointer
##  params  void
##  returns char* %rax
name:
        leaq    _str_name(%rip), %rax
        ret

## Retruns color corresponding to %rdi
##  params  int   %rdi
##  returns int32 %rax
color:
        movq    $4, %rax
        imulq   %rdi            # i*4
        leaq    _colors(%rip), %rcx
        addq    %rax, %rcx      # colors[index]
        movl    (%rcx), %eax
        ret

## Initialize game env
##  params  short* %rdi (screen)
##  params  short* %rsi (input)
##  returns int
init:
        movq    %rdi, %rax
        cmpq    $0, %rax        # NULL pointer
        je      init_end_error
        ## Register screen ptr
        leaq    _screen(%rip), %rax
        movq    %rdi, (%rax)

        movq    %rsi, %rax
        cmpq    $0, %rax        # NULL pointer
        je      init_end_error
        ## Register input ptr
        leaq    _input(%rip), %rax
        movq    %rsi, (%rax)

init_end:
        leaq    _status(%rip), %rax
        movq    $0, (%rax)

        movq    $0, %rax
        ret
init_end_error:
        movq    $-1, %rax
        ret


## Render new frame
##  params  void
##  retruns int
render:
        ## Read keys
        leaq    _input(%rip), %rcx
        cmpq    $0, (%rcx)      # NULL ptr ?
        je      render_end_error
        movq    (%rcx), %rdx    # Get input table ptr

        ## Check keys, and move rect
render_key_up:
        mov     (%rdx), %ax
        cmp     $0, %ax         # KEY_UP
        je      render_key_down # Goto next key test

        leaq    _p1_y(%rip), %rax  # Get y value
        movq    (%rax), %rcx
        cmpq    $0, %rcx        # Out of tab
        jle     render_key_down
        subq    $2, %rcx        # Move on screen
        movq    %rcx, (%rax)

render_key_down:
        addq    $2, %rdx
        mov     (%rdx), %ax
        cmp     $0, %ax         # KEY_DOWN
        je      render_key_end

        leaq    _p1_y(%rip), %rax
        movq    (%rax), %rcx
        cmpq    $214, %rcx
        jge     render_key_end
        addq    $2, %rcx
        movq    %rcx, (%rax)
render_key_end:

        ## Paddle
        leaq    _paddle(%rip), %rdi
        movq    $0, %rsi
        leaq    _p1_y(%rip), %rcx  # y
        movq    (%rcx), %rdx
        movq    $4, %rcx        # Color 4
        call    draw_tex_with_color

        ## Paddle 2
        leaq    _paddle(%rip), %rdi
        movq    $314, %rsi
        leaq    _p2_y(%rip), %rcx
        movq    (%rcx), %rdx    # p2 y
        movq    $2, %rcx        # Color 2
        call    draw_tex_with_color

        ## Ball
        leaq    _ball(%rip), %rdi
        leaq    _ball_x(%rip), %rax # x
        movq    (%rax), %rsi
        leaq    _ball_y(%rip), %rax # y
        movq    (%rax), %rdx
        call    draw_tex

move_ball_x:
        leaq    _ball_x(%rip), %rcx
        movq    (%rcx), %rax    # _ball_x value
        leaq    _ball_speed_x(%rip), %rdx
        movq    (%rdx), %rdi    # _ball_speed_x
        addq    %rdi, %rax
        cmpq    $0, %rax        # touch border
        jle     move_ball_x_touch_left
        cmpq    $6, %rax        # Player 1
        jle     move_ball_x_touch_player1
        cmpq    $312, %rax      # touch border
        jge     move_ball_x_touch_right
        cmpq    $306, %rax      # Player 2
        jge     move_ball_x_touch_player2
        jmp     move_ball_x_normal_end
move_ball_x_touch_player1:
        leaq    _ball_y(%rip), %r8
        movq    (%r8), %rsi     # Ball y
        addq    $4, %rsi        # Center of ball
        leaq    _p1_y(%rip), %r8
        movq    (%r8), %r8      # Player y
        addq    $12, %r8        # Center of paddle
        subq    %rsi, %r8       # Diff betbeew player and ball pos
        cmpq    $16, %r8
        jge     move_ball_x_normal_end
        cmpq    $-16, %r8
        jle     move_ball_x_normal_end
        ## Reverse dir
        movq    %rax, (%rcx)    # Save new pos
        xor     %rax, %rax
        subq    %rdi, %rax
        movq    %rax, (%rdx)    # speed = -speed
        jmp     move_ball_x_end
move_ball_x_touch_player2:
        leaq    _ball_y(%rip), %r8
        movq    (%r8), %rsi     # Ball y
        addq    $4, %rsi        # Center of ball
        leaq    _p2_y(%rip), %r8
        movq    (%r8), %r8      # Player y
        addq    $12, %r8        # Center of paddle
        subq    %rsi, %r8
        cmpq    $16, %r8
        jge     move_ball_x_normal_end
        cmpq    $-16, %r8
        jle     move_ball_x_normal_end
        ## Reverse dir
        movq    %rax, (%rcx)    # Save new pos
        xor     %rax, %rax
        subq    %rdi, %rax
        movq    %rax, (%rdx)    # speed = -speed
        jmp     move_ball_x_end
move_ball_x_touch_right:
        leaq    _score_p1(%rip), %rax
        movq    (%rax), %rcx
        addq    $1, %rcx
        movq    %rcx, (%rax)

        call    reset_game
        jmp     move_ball_x_end
move_ball_x_touch_left:
        leaq    _score_p2(%rip), %rax
        movq    (%rax), %rcx
        addq    $1, %rcx
        movq    %rcx, (%rax)

        call    reset_game
        jmp     move_ball_x_end
move_ball_x_normal_end:
        movq    %rax, (%rcx)    # Save new pos
move_ball_x_end:


move_ball_y:
        leaq    _ball_y(%rip), %rcx
        movq    (%rcx), %rax    # _ball_y value
        leaq    _ball_speed_y(%rip), %rdx
        movq    (%rdx), %rdi    # _ball_speed_y
        addq    %rdi, %rax
        cmpq    $232, %rax
        jge     move_ball_y_touch_bottom
        cmpq    $0, %rax
        jle     move_ball_y_touch_top
        movq    %rax, (%rcx)    # Save new pos
        jmp     move_ball_y_end
move_ball_y_touch_bottom:
        movq    $232, (%rcx)    # Save new pos
        xor     %rax, %rax
        subq    %rdi, %rax
        movq    %rax, (%rdx)    # speed = -speed
        jmp     move_ball_y_end
move_ball_y_touch_top:
        movq    $0, (%rcx)      # Save new pos
        xor     %rax, %rax
        subq    %rdi, %rax
        movq    %rax, (%rdx)    # speed = -speed
        jmp     move_ball_y_end
move_ball_y_end:

        ## display score
score:
        leaq    _score_p1(%rip), %rax
        movq    (%rax), %rdi
        call    get_num_tex
        movq    %rax, %rdi
        movq    $150, %rsi
        movq    $5, %rdx
        call    draw_tex

        leaq    _score_p2(%rip), %rax
        movq    (%rax), %rdi
        call    get_num_tex
        movq    %rax, %rdi
        movq    $170, %rsi
        movq    $5, %rdx
        call    draw_tex

        movq    $164, %rdi
        movq    $5, %rsi
        call    draw_vertical_line_8

count_frame:
        leaq    _frames(%rip), %rax
        movq    (%rax), %rcx
        addq    $1, %rcx
        movq    %rcx, (%rax)

render_end:
        xor     %rax, %rax
        ret
render_end_error:
        movq    $-1, %rax
        ret

# pong.s ends here.
