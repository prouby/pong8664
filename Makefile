CC = gcc
AS = $(CC)
LD = $(CC)
CFLAGS = -Wall -fPIC
DEBUG = -g

all: pong.so debug

pong.o: pong.s sprites.s colors.s display.s
	@echo " AS    $@"
	@$(AS) $(CFLAGS) -c $< -o $@

pong.so: pong.o
	@echo " LD    $@"
	@$(LD) -fPIC --share -o $@ $<


dbg_pong.o: pong.s sprites.s colors.s display.s
	@echo " AS    $@"
	@$(AS) $(CFLAGS) $(DEBUG) -c $< -o $@

dbg_pong.so: dbg_pong.o
	@echo " LD    $@"
	@$(LD) $(DEBUG) -fPIC --share -o $@ $<

debug: dbg_pong.so


clean:
	@echo " CLEAN"
	@rm -v pong.so dbg_pong.so *.o

.PHONY: all clean
