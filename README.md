# Pong 8664

## Dependencies

 - Register game launcher: https://framagit.org/prouby/register/
 - x86_64 processor

## Build it

```shell
make
```

## Run it

```shell
register ./pong.so
```
